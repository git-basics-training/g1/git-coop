# git-coop

- Pracujemy w projekcie git-basics-training/g1/git-coop na serwerze GitLab
- W projekcie tym jest stworzone 70 zadań
- Każda osoba powinna rozwiązać 5 zadań (podejmując zadanie przypiszcie je do siebie aby inni widzieli, że zajęte)
- Każde zadanie posiada swój opis
- Dla każdego issue należy stworzyć Feature Branch bazujący na dev
-- Najlepiej tworzyć branch z poziomu issue w GitLab
- Każdą zmianę należy następnie zgłosić jako Pull Request do branch’a dev
-- Każda osoba musi wykonać min. 5 review i akceptację Pull Request’ów innych
-- Strategię podziału kto wykonuje review ustalcie między sobą
- W momencie wystawienia Pull Request’a podejmujemy kolejne zadanie i je realizujemy, nie czekamy na merge
- Wszystkie Commit Message muszą jasno wskazywać jaka zmiana była dokonana
- Po wykonaniu wszystkich zmian jedna wyznaczona przez Was osoba wystawia PullRequest z branch’a dev do master


## Pliki które znajdują się w repozytorium trafią na koniec do excel’a gdzie jest gotowa formuła oraz poprawny wynik. Jeśli wszystkie operacje zostały wykonane poprawnie oraz wszelkie potencjalne konflikty również zostały poprawnie rozwiązane to wyniki będą się zgadzały!

